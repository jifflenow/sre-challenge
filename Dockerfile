FROM store/oracle/serverjre:8
ADD helloworld.war /usr/local/tomcat/webapps/ROOT.war

ENTRYPOINT ["java","-jar","/usr/local/tomcat/webapps/ROOT.war"]

