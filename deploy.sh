#!/bin/sh
BRANCH=""
printhelp() {

echo "
Usage: sh deploy.sh [OPTION]...
  -b, --branch   Provide your git branch to deploy.
  -h, --help Display help file
"

}
while [ "$1" != "" ]; do
  case "$1" in
    -b | --branch )       BRANCH=$2; shift 2 ;;
    -h | --help )	        echo "$(printhelp)"; exit; shift; break ;;
  esac
done

kubectl set image deployment/tomcat-ssl tomcat-ssl=registry.gitlab.com/jifflenow/sre-challenge:$BRANCH
kubectl rollout status -w deployment/tomcat-ssl
