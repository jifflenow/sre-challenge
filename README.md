# sre-challenge

The scenario mentioned in the task was an old technique, maybe used 3 or 4 years back. During those periods there weren't any good orchestration tools for docker.
Nowadays you can see a lot of tools like Kubernetes, DC/OS, DockerSwarm, Ranger, Nomad etc for docker orchestration.

I am using AWS EKS for this process. I created this repo using my technique which I created in last year.

https://github.com/vishnudxb/docker-eks

You can find the video of creating EKS in below link

https://asciinema.org/a/UufbR6DJo7JQO4mByrZnHfsBn

You can manage the EKS Worker nodes and VPC using the cloudformation template which is automatically created using my docker-eks repo.
To interact with the EKS, you need to login to the docker container.

I am adding an extra layer of security on top of the AWS EKS by managing the installation dependencies inside a container.

Please check the sre-solutions.pdf file for more details. 
