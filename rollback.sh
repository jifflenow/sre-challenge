#!/bin/sh
echo "Rolling back to the previous deployment"

kubectl rollout undo deployment/tomcat-ssl
kubectl rollout status -w deployment/tomcat-ssl
